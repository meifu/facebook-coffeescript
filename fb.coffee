app_id = '511239108925093'
redirect_uri = location.href

u_id = ""
userName = ""
userEmail = ""
isFans = 'undefined'
fbaccess_token = ''

fbLikeBtn_id = 'fbLikeBtn'
page_fan_name = "家樂福 Carrefour"
page_fan_id = "170290829685289"


window.fbAsyncInit = () ->

	FB.init(
		{appId: app_id, channelURL: '', status: true, cookie: true, oauth: true, xfbml: true}
	)
	console.log('fbasync')
	FB.getLoginStatus(
		(rep) -> 
			if rep.status is 'connected'
				alreadyConnected() 
			else 
				toLoginProcess()
			return false
	)

alreadyConnected = () ->
	console.log('login')
	isFan()

#去做login and authenticate
toLoginProcess = () ->
	console.log('should go to login')
	loginURL = 'https://www.facebook.com/dialog/oauth/?' + 'scope=email,user_likes,publish_stream,user_photos&' + 'client_id=' + app_id + '&' + 'redirect_uri=' + redirect_uri
	location.href = loginURL

#判斷是否為fans
isFan = () ->
	FB.api(
		{method: 'pages.isFan', page_id: page_fan_id, uid: u_id},
		(resp) ->
			if resp is true
				isFans = true
				shareToFB()
			else if resp is false
				isFans = false
				joinFans()
	)

#加入fans
joinFans = () ->
	console.log('join' + FB)
	console.log('fb button: ' + document.getElementById(fbLikeBtn_id).style.display)
	document.getElementById(fbLikeBtn_id).style.display = 'block'
	FB.Event.subscribe('edge.create', (response) -> 
		console.log('join fanpage success')
	)
	FB.Event.subscribe('edge.remove', (response) ->
		location.reload()
	)

#分享FB ui feed
shareToFB = () ->
	FB.ui(
		{
			method: 'feed' 
			link: 'http://coffeescript.org/' 
			name: 'CoffeeScript' 
			caption: 'CoffeeScript is a good thing' 
			picture: 'http://bodil.org/coffeescript/media/coffeescript_logo.png'
			description: 'Let us use coffeescript to think about javascript'
		},
		(resp) ->
			if resp isnt null
				console.log('share success')
			else
				console.log('please share')
	)

# Load the SDK Asynchronously
do ( ->
	js = document.createElement('script')
	js.id = 'facebook-jssdk'
	js.async = true
	js.src = "//connect.facebook.net/zh_TW/all.js"
	document.getElementsByTagName('head')[0].appendChild(js)
)